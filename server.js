var express = require('express');
var firebase = require('firebase');

const body_parser = require('body-parser');

var app = express();
app.use(body_parser.json());
var firebaseConfig = {
    apiKey: "AIzaSyDSLvJ0htJA0Rwa1m7FSl06eowAWXRENyQ",
    authDomain: "testappsnsites.firebaseapp.com",
    databaseURL: "https://testappsnsites.firebaseio.com",
    projectId: "testappsnsites",
    storageBucket: "testappsnsites.appspot.com",
    messagingSenderId: "448889722967",
    appId: "1:448889722967:web:3659b9c9d93b00836b2b12",
    measurementId: "G-B1RJN7P4Z4"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// listing profiles
app.get('/profiles', function (req, res) {
    firebase.database()
        .ref('testappsnsites')
        .child('profiles')
        .on('value', r => {
        var profiles = [];

        Object.keys(r.val()).forEach(k =>
            {
                var p = r.val()[k];
                p.uuid = k;
            profiles.push(p);
        }
        );
        res.send(profiles);
    });
});
//getting profile by uuid
app.get('/profiles/:uuid', function (req, res) {
    var uuid = req.params.uuid;
  res.send(getProfile(uuid));

});
function getProfile(uuid){
    var profile;
    firebase.database().ref('testappsnsites').child('profiles').child(uuid).on('value', r => {
         profile = r.val();
        profile.uuid = uuid;

    });
    return profile;
}
//update profile
app.put('/profiles/:uuid', function (req, res) {
    var profile = req.body;
    var profileKeys = Object.keys(profile);
    if(profileKeys.indexOf('firstname') < 0 || profileKeys.indexOf('lastname')<0 || profileKeys.indexOf('email')<0){
        res.status(400).send({error: 'bad request'});
    }else {
        firebase.database()
            .ref('testappsnsites')
            .child('profiles')
            .child(req.params.uuid)
            .set(req.body)
            .then(value => res.send(getProfile(req.params.uuid)));
    }
});
//add profile
app.post('/profiles', function (req, res) {
    var profile = req.body;
    var profileKeys = Object.keys(profile);
    if(profileKeys.indexOf('firstname') < 0 || profileKeys.indexOf('lastname')<0 || profileKeys.indexOf('email')<0){
        res.status(400).send({error: 'bad request'});
    }else {
        var newProfile =  firebase.database().ref('testappsnsites').child('profiles').push(req.body);
       res.send(getProfile(newProfile.key));
    }

});

app.delete('/profiles/:uuid', function (req, res) {

firebase.database().ref('testappsnsites').child('profiles').child(req.params.uuid).remove(r => res.send(true));
});
var server = app.listen(8080, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log("server listening at http://%s:%s", host, port);
});


